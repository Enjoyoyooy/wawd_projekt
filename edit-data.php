<?php
error_reporting(0);
include_once 'dbconfig.php';
if(isset($_POST['btn-update']))
{
	$id = $_GET['edit_id'];
	$tname = $_POST['task_name'];
	$tproject = $_POST['task_project'];
	$crdate = $_POST['creation_date'];
	$duedate = $_POST['due_date'];
	$comment = $_POST['comment'];
	$createdby = $_POST['created_by'];
	$assignedto = $_POST['assigned_to'];
	$status = $_POST['status'];
	
	if($crud->update($id,$tname,$tproject,$crdate,$duedate,$comment,$createdby,$assignedto,$status))
	{
		$msg = "<div class='alert alert-info'>
				<strong>WOW!</strong> Record was updated successfully <a href='index.php'>HOME</a>!
				</div>";
	}
	else
	{
		$msg = "<div class='alert alert-warning'>
				<strong>SORRY!</strong> ERROR while updating record !
				</div>";
	}
}

if(isset($_GET['edit_id']))
{
	$id = $_GET['edit_id'];
	extract($crud->getID($id));	
}

?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
	echo $msg;
}
?>
</div>

<div class="clearfix"></div><br />

<div class="container">
	 
     <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Task Name</td>
            <td><input type='text' name='task_name' class='form-control' value="<?php echo $task_name; ?>" required></td>
        </tr>
 
        <tr>
            <td>Task Project</td>
            <td><input type='text' name='task_project' class='form-control' value="<?php echo $task_project; ?>" required></td>
        </tr>
 
        <tr>
            <td>Creation Date</td>
            <td><input type='date' name='creation_date' class='form-control' value="<?php echo $creation_date; ?>" required></td>
        </tr>
 
        <tr>
            <td>Due Date</td>
            <td><input type='date' name='due_date' class='form-control' value="<?php echo $due_date; ?>" required></td>
        </tr>
		
		<tr>
            <td>Comment</td>
            <td><input type='text' name='comment' class='form-control' value="<?php echo $comment; ?>" required></td>
        </tr>
		
		<tr>
            <td>Created By</td>
            <td><input type='text' name='created_by' class='form-control' value="<?php echo $created_by; ?>" required></td>
        </tr>
		
		<tr>
            <td>Assigned To</td>
            <td><input type='text' name='assigned_to' class='form-control' value="<?php echo $assigned_to; ?>" required></td>
        </tr>
		
		<tr>
            <td>Status</td>
            <td><input type='text' name='status' class='form-control' value="<?php echo $status; ?>" required></td>
        </tr>
 
        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
    			<span class="glyphicon glyphicon-edit"></span>  Update this Record
				</button>
                <a href="index.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>

<?php include_once 'footer.php'; ?>