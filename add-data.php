<?php
include_once 'dbconfig.php';
if(isset($_POST['btn-save']))
{
	$tname = $_POST['task_name'];
	$tproject = $_POST['task_project'];
	$crdate = $_POST['creation_date'];
	$duedate = $_POST['due_date'];
	$comment = $_POST['comment'];
	$createdby = $_POST['created_by'];
	$assignedto = $_POST['assigned_to'];
	$status = $_POST['status'];
	
	if($crud->create($tname,$tproject,$crdate,$duedate,$comment,$createdby,$assignedto,$status))
	{
		header("Location: add-data.php?inserted");
	}
	else
	{
		header("Location: add-data.php?failure");
	}
}
?>
<?php include_once 'header.php'; ?>
<div class="clearfix"></div>

<?php
if(isset($_GET['inserted']))
{
	?>
    <div class="container">
	<div class="alert alert-info">
    <strong>WOW!</strong> Record was inserted successfully <a href="index.php">HOME</a>!
	</div>
	</div>
    <?php
}
else if(isset($_GET['failure']))
{
	?>
    <div class="container">
	<div class="alert alert-warning">
    <strong>SORRY!</strong> ERROR while inserting record !
	</div>
	</div>
    <?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

 	
	 <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Task Name</td>
            <td><input type='text' name='task_name' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Task Project</td>
            <td><input type='text' name='task_project' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Creation Date</td>
            <td><input type='date' name='creation_date' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Due Date</td>
            <td><input type='date' name='due_date' class='form-control' required></td>
        </tr>
		
		<tr>
            <td>Comment</td>
            <td><input type='text' name='comment' class='form-control' required></td>
        </tr>
		
		<tr>
            <td>Created By</td>
            <td><input type='text' name='created_by' class='form-control' required></td>
        </tr>
		
		<tr>
            <td>Assigned To</td>
            <td><input type='text' name='assigned_to' class='form-control' required></td>
        </tr>
		
		<tr>
            <td>Status</td>
            <td><input type='text' name='status' class='form-control' value='New' required></td>
        </tr>
 
        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
    		<span class="glyphicon glyphicon-plus"></span> Create task
			</button>  
            <a href="index.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Go back</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>

<?php include_once 'footer.php'; ?>