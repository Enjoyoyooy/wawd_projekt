<?php
//error_reporting(0);
include_once 'dbconfig.php';
?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<a href="add-project.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Project</a>
</div>

<div class="clearfix"></div><br />

<div class="container">
	 <table class='table table-bordered table-responsive'>
     <tr>
     <th>#</th>
     <th>Task Project</th>
	 <th>Creation Date</th>
	 <th colspan="2" align="center">Actions</th>
     </tr>
     <?php
		$query = "SELECT ID, project_name, creation_date FROM tbl_projects";
		$records_per_page=3;
		$newquery = $crud->paging($query,$records_per_page);
		$crud->projectview($newquery);
	 ?>
    <tr>
        <td colspan="6" align="center">
 			<div class="pagination-wrap">
            <?php $crud->paginglink($query,$records_per_page); ?>
        	</div>
        </td>
    </tr>
 
</table>
   
       
</div>

<?php include_once 'footer.php'; ?>