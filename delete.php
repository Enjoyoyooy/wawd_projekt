<?php
include_once 'dbconfig.php';

if(isset($_POST['btn-del']))
{
	$id = $_GET['delete_id'];
	$crud->delete($id);
	header("Location: delete.php?deleted");	
}

?>

<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">

	<?php
	if(isset($_GET['deleted']))
	{
		?>
        <div class="alert alert-success">
    	<strong>Success!</strong> record was deleted... 
		</div>
        <?php
	}
	else
	{
		?>
        <div class="alert alert-danger">
    	<strong>Sure !</strong> to remove the following record ? 
		</div>
        <?php
	}
	?>	
</div>

<div class="clearfix"></div>

<div class="container">
 	
	 <?php
	 if(isset($_GET['delete_id']))
	 {
		 ?>
		 <table class='table table-bordered'>
		 <tr>
		 <th>#</th>
		 <th>Task Name</th>
		 <th>Task Project</th>
		 <th>Creation Date</th>
		 <th>Due Date</th>
		 <th>Comment</th>
		 <th>Created By</th>
		 <th>Assigned To</th>
		 <th>Status</th>
		 </tr>
		 <?php
		 $stmt = $DB_con->prepare("SELECT * FROM tbl_users WHERE ID=:id");
		 $stmt->execute(array(":id"=>$_GET['delete_id']));
		 while($row=$stmt->fetch(PDO::FETCH_BOTH))
		 {
			 ?>
			 <tr>
			 <td><?php print($row['ID']); ?></td>
			 <td><?php print($row['task_name']); ?></td>
			 <td><?php print($row['task_project']); ?></td>
			 <td><?php print($row['creation_date']); ?></td>
			 <td><?php print($row['due_date']); ?></td>
			 <td><?php print($row['comment']); ?></td>
			 <td><?php print($row['created_by']); ?></td>
			 <td><?php print($row['assigned_to']); ?></td>
			 <td><?php print($row['status']); ?></td>
			 </tr>
			 <?php
		 }
		 ?>
		 </table>
		 <?php
	 }
	 ?>
</div>

<div class="container">
<p>
<?php
if(isset($_GET['delete_id']))
{
	?>
  	<form method="post">
    <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
    <button class="btn btn-large btn-primary" type="submit" name="btn-del"><i class="glyphicon glyphicon-trash"></i> &nbsp; YES</button>
    <a href="index.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
    </form>  
	<?php
}
else
{
	?>
    <a href="index.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to index</a>
    <?php
}
?>
</p>
</div>	
<?php include_once 'footer.php'; ?>